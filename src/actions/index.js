import fetchJsonp from 'fetch-jsonp';
import postcode from 'postcode-validator';

import { UPDATE_MP_LIST, ERROR_MP_LIST, FETCHING_MP_LIST } from '../constants';
import Config from '../config';

export const callApi = (postalCode, dispatch) => {

  // Remove all spaces in the postal code so the user
  // does not have to worry about this on entry
  postalCode = postalCode.replace(/ /g,'');
  
  // Check if the data postal code is valid
  // I use another npm package here as writing
  // all the edge cases would take too long
  if (!postcode.validate(postalCode, 'CA')) {
    // Dispatch error if invalid postcode
    dispatch({
      type: ERROR_MP_LIST,
      payload: {
        errorMessages: ["Invalid Postcode"]
      }
    });
    // Break out of the function if this does not validate
    return;
  }

  // Diapatch to show loading on screen
  dispatch({
    type: FETCHING_MP_LIST,
  });

  // Fetch the data using the config
  fetchJsonp(Config.api + postalCode, {
    timeout: 3000,
  })
  .then(function(response) {
    return response.json();
  }).then(function(json) {
    // Dispatch with the contents
    dispatch({
      type: UPDATE_MP_LIST,
      payload: {
        loading: false,
        mpList: json.representatives_centroid.filter(item => item.elected_office === "MP") // Filter MPs
      }
    });
  }).catch(function(ex) {
    // Dispatch default error
    dispatch({
      type: ERROR_MP_LIST,
      payload: {
        errorMessages: ["There was an issue getting the data"]
      }
    });
  })
};
