import { UPDATE_MP_LIST, ERROR_MP_LIST, FETCHING_MP_LIST } from '../constants';

const initialState = {
  mpList: [],
  loading: false,
  errorMessages: []
};

// Simple reducer to handle the actions
const mpReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_MP_LIST:
      return {
        mpList: action.payload.mpList,
        loading: false,
        errorMessages: []
      };
    case ERROR_MP_LIST:
      return {
        ...state,
        loading: false,
        errorMessages: action.payload.errorMessages
      };
    case FETCHING_MP_LIST:
      return {
        ...state,
        errorMessages: [],
        loading: true,
      };
    default:
      return state;
  }
};

export default mpReducer;