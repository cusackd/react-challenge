import React, { Component } from 'react';
import { connect } from 'react-redux';

import logo from './assets/logo.svg';
import './assets/App.css';

import { callApi } from './actions';

import LoadingComponent from './components/Loading';
import ErrorMessagesComponent from './components/ErrorMessages';
import MpListComponent from './components/MpList';


class App extends Component {
  // Set state of postal code in the component
  // I left this here to show my understanding of state
  // on a component level
  state = {
    postalCode: ""
  }

  // Update state based on the html id
  // I would probably handle this state in
  // the reducer upon refactor or more time
  _updateState(e) {
    // Force uppercase on the entry
    e.target.value =  e.target.value.toUpperCase();
    this.setState({
      [e.target.id]: e.target.value
    });
  }
  
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">React Tech Challenge</h1>
        </header>
        <p className="App-intro">
          Enter your postal code to find your MP!
          <br />
          <input id="postalCode" value={this.state.postalCode} placeholder="V2J 7C2" onChange={(e) => this._updateState(e)} />
          <br />
          <button onClick={() => this.props.callApi(this.state.postalCode)}>Go</button>
        </p>
          {this.props.loading && <LoadingComponent />}
          {this.props.errorMessages && <ErrorMessagesComponent errorMessages={this.props.errorMessages} />}
          {this.props.mpList && <MpListComponent mpList={this.props.mpList} />}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  mpList: state.mpList,
  loading: state.loading,
  errorMessages: state.errorMessages
});

const mapDispatchToProps = (dispatch) => ({
  callApi: (postalCode) => callApi(postalCode, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
