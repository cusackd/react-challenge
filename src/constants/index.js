// Using constants so we can easily see what is broken
// and ensure there is no typos on the actions
export const UPDATE_MP_LIST = 'UPDATE_MP_LIST';
export const ERROR_MP_LIST = 'ERROR_MP_LIST';
export const FETCHING_MP_LIST = 'FETCHING_MP_LIST';