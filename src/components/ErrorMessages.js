import React from 'react';

// Stateless component to handle messages
const ErrorMessagesComponent = (props) => {
  return props.errorMessages.map((item, index) => (
    <div className="errorMessage" key={index}>
        {item}
    </div>
  ));
}

export default ErrorMessagesComponent;