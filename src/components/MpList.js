import React from 'react';

// Stateless component to display MPs
const MpListComponent = (props) => {
  return props.mpList.map((item, index) => (
    <div className="mpListItem" key={index}>
      <div className="name">{item.first_name} {item.last_name}</div>
      <div className="email"><a href={`mailto:${item.email}`}>{item.email}</a></div>
    </div>
  ));
}


export default MpListComponent;