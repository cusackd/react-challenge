import React from 'react';

// Stateless component to show loading
const LoadingComponent = (props) => {
  return (
    <div className="loading">Loading</div>
  )
}


export default LoadingComponent;